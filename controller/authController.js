const User = require("../model/userModel.js");
const { promisify } = require("util");
const crypto=require("crypto")
require("dotenv");
const jwt = require("jsonwebtoken");
const CatchAsync = require("../utils/catchAsync.js");
const AppError = require("../utils/appError.js");
const catchAsync = require("../utils/catchAsync.js");
const sendEmail = require("../utils/email.js");

const singleToken = (id) => {
  return jwt.sign({ id: id }, process.env.JWT_SECERT, {
    expiresIn: process.env.JWT_EXPIRES_IN,
  });
};


const createSendToken=(user,statusCode,res)=>{
  const token = singleToken(user._id);

  res.cookie('jwt',token,{
    expires:new Date(
      Date.now() + process.env.COOKIES_EXPIRES *24*60*60*1000
    ),
   // secue:true,
    httpOnly:true,
  })

  //remove password from output
  user.Password=undefined
  res.status(statusCode).json({
    status:"Passed",
    token,
    data:user
  })
}

const signup = CatchAsync(async (req, res, next) => {
 //console.log(req.body) 
  const newUser = await User.create({
    name: req.body.name, 
    email: req.body.email,
    Password: req.body.Password,
    comfirmPassword: req.body.comfirmPassword,
    passwordChangeAT: req.body.passwordChangeAT,
    role: req.body.role, // define the  field on the page route also to get the enum value , not default
  });
 

  // if (!token) {
  //   return next(new AppError("Token generation Error", 400));
  // }

  createSendToken(newUser,200,res)
});

const login = CatchAsync(async (req, res, next) => {
  // try {
  const { email, Password } = req.body;
  if (!email || !Password) {
    return next(new AppError("Please enter Email and password", 404));
  }
  const user = await User.findOne({ email }).select("+Password");
 // console.log(user)
  if (!user || !(await user.correctPassword(Password, user.Password))) {
    return next(new AppError("Incorrect Email and Pasword"), 500);
  }

 // const token = singleToken(user.id);
 
  // res.status(200).json({
  //   status: "Pass",
  //   token,
  // });

  createSendToken(user,200,res)
});

const protectAll = catchAsync(async (req, res, next) => {
  let token = "";
  // getting token and verify the token
  // console.log(req.headers.authorization)
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith("Bearer")
  ) {
    token = req.headers.authorization.split(" ")[1];
  }else if(req.cookie.token){
      token=req.cookie.token
  }
  //console.log(token)
  if (!token) {
    return next(new AppError("Logging again!!!!!!", 404));
  }
  // virefacation of token
  const decode = await promisify(jwt.verify)(token, process.env.JWT_SECERT);
//console.log(decode)
  //check if user still exist
  const freshID = await User.findById(decode.id);//decode.id  is  uers.id
  //console.log("User=====",decode.id)
  if (!freshID) {
    return next(new AppError("ID not found,Please SignUP !!!!! "), 404);
  }

  // check if user change pwd after the token was isseued
  if (freshID.passwordChanged(decode.iat)) {// The iat claim is a standard JWT claim that represents the timestamp (in seconds) when the token was issued. 
    return next(
      new AppError("Password is changed,Please enter the password again"),
      404
    );
  }
  req.user = freshID

  next();
});

const restrictTo = (...roles) => {
  return (req, res, next) => {
    if (!roles.includes(req.user.role)) {
      return next(new AppError("You do not have the premission!!", 404));
    }
    next();
  };
};

const forgotPwd = catchAsync(async (req, res, next) => {
  //get user based post email
  const user = await User.findOne({ email: req.body.email });

  if (!user) {
    return next(new AppError("No user found!! Signup ", 404));
  }
  // generted the user rest token
  const restToken = user.restpwdToken();
  await user.save({ validateBeforeSave: false });
  //send it to the user email

  const restURL = `${req.protocol}://${req.get(
    "host"
  )}/api/v1/users/rest/${restToken}`;

  const message = `Forgot your password? Submit a PATCH request with your new password and passwordConfirm to: ${restURL}.\nIf you didn't forget your password, please ignore this email!`;
 
  try {
    await sendEmail({
      email: user.email,
      subject: "Your password reset token (valid for 10 min only)",
      message,
    });
    res.status(200).json({
      status: "success",
      message: "Token sent via email!!!!!",
    });
  } catch (error) {
    (user.passwordRestToken = undefined),
      (user.passwordRestExpried = undefined);
    await user.save({ validateBeforeSave: false });
    return next(new AppError("Error while sending a email Try again!!"), 404);
  }
});


const restPwd =catchAsync(async (req, res, next) => {
  // get user based on the token 
  const hashToken = crypto.createHash('sha256').update(req.params.token).digest('hex');
    const user= await  User.findOne({passwordRestToken: hashToken, passwordRestExpried:{$gt: Date.now()} })
//console.log(user)
  //if token is not expired and there is user set the password 
  if(!user){
    return next(new AppError("Token is expired or invalid token!!"),404)
  }

  user.Password=req.body.Password
  user.comfirmPassword=req.body.comfirmPassword
  user.passwordRestToken=undefined
  user.passwordRestExpried=undefined

  await user.save()
  //  update the change password  property for the user

  //log user in send jwt
  // const token = singleToken(user.id);
  // res.status(200).json({
  //   status: "Pass",
  //   token,
  // });

  createSendToken(user,200,res)


});


const updatePasword=catchAsync(async (req,res,next)=>{
  // get user from collection
  const user= await user.findOne(req.body.id).select("+Password")
  //check if posted current pwd is corrected
  if(!(user.correctPassword(req.body.passwordCurrent,user.Password))){
  return next(new AppError("Your Current Password is wrong",400))
  }
  // if so pwd issues
  user.Password=req.body.Password
  user.comfirmPassword=req.body.comfirmPassword
  await user.save()
  //log user in,  send jwt
  createSendToken(user,200,res)
})






module.exports = {
  signup,
  login,
  protectAll,
  restrictTo,
  forgotPwd,
  restPwd,
  updatePasword,
};
