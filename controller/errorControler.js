const AppError = require("../utils/appError")

const handleDulpicateField=(err)=>{
  const value=err.errmsg.match(/(["'])(\\?.)*?\1/)
 // console.log(e)
  const message=`Duplicate Value of ${value}`

  return (new AppError(message,404))

}

const sendErrorProd=(err,res)=>{
  if(err.Opera){
    res.status(err.statusCode).json({
      status : err.status,
      message : err.message
    })
  }else{
    console.error("Error",err)
  }
}

module.exports =(err,req,res,next)=>{
 // console.log(err.stack)

  err.statusCode=err.statusCode || 500
  err.status=err.status || "error"

  if(err.code == 11000) err=handleDulpicateField(err)

  res.status(err.statusCode).json({
    status:err.status,
    message:err.message
  })

}








