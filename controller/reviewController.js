const reviewdb = require("../model/reviewModel");
const { listenerCount } = require("../model/toursModel.js");
const catchAsync = require("../utils/catchAsync");
const MainFunction = require("../utils/handlerController.js");
exports.setTourUserIds = (req, res, next) => {
  // Allow nested routes
  if (!req.body.tour) req.body.tour = req.params.tourId;
  if (!req.body.user) req.body.user = req.user.id;
  next();
};


exports.getAllReview = catchAsync(async (req, res, next) => {
  let filter ={} 
  if (req.params.tourId) filter = { tour: req.params.tourId };
  const result = await reviewdb.find(filter);
  res.status(201).json({
    msg: "Fetch the data successfully",
    Total: result.length,
    result,
  });
});

exports.addReview = catchAsync(async (req, res, next) => {


  const { review, rating, createdAt, tour, user } = req.body;

  const result = await reviewdb.create({
    tour,
    user,
    review,
    rating,
    createdAt,
  });

  res.status(200).json({
    msg: "new review added",
    result,
  });
});

exports.getSingle = catchAsync(async (req, res, next) => {
  const { id } = req.params;
  const result = await reviewdb.findById(id);
  if(!result){
    throw new Error("review not found")
  }

  res.status(200).json({
    msg: "Find the data successfully",
    result,
  });
});

exports.updateReview = catchAsync(async (req, res, next) => {
  const { id } = req.params;
  const { review, rating } = req.body;
  const updatereview = await reviewdb.findByIdAndUpdate(id, {
    review,
    rating,
  });

  if (!updatereview) {
    throw new Error("review not found");
  }

  res.status(201).json({
    msg: "updated review",
    result: null,
  });
});

exports.removeReview = MainFunction.deleteOne(reviewdb);
