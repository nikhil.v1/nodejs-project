

const ToursDB = require("../model/toursModel.js");
const CatchAsync=require("../utils/catchAsync.js")
const AppError = require("../utils/appError.js")
const MainFunction=require("../utils/handlerController.js");
const catchAsync = require("../utils/catchAsync.js");
class APIFeatures {
  constructor(query, queryString) {
    (this.query = query), (this.queryString = queryString);
  }

  filter() {
    const queryObj = { ...this.queryString };
    const excludeFiled = ["page", "sort", "limit", "fields"];
    excludeFiled.forEach((ele) => delete queryObj[ele]);
    // advance filtering like  greater than , less than
    let queryStr = JSON.stringify(queryObj);
    queryStr = queryStr.replace(/\b(gte|gt|lte|lt)\b/g, (match) => `$${match}`);
    // console.log(JSON.parse(queryStr))
    this.query.find(JSON.parse(queryStr));
    // let query =   ToursDB.find()
    return this;
  }

  sort() {
    if (this.queryString.sort) {
      const sortBy = this.queryString.sort.split(",").join(" ");
      this.query = this.query.sort(sortBy);
    } else {
      this.query = this.query.sort("-createdAt");
    }
    return this;
  }

  pagination() {
    const page = this.queryString.page * 1 || 1;
    const limit = this.queryString.limit * 1 || 100;
    const skip = (page - 1) * limit;

    this.query = this.query.skip(skip).limit(limit);

    return this;
  }

  limitedFeilds() {
    if (this.queryString.fields) {
      const feildsBy = this.queryString.fields.split(",").join(" ");
      this.query = this.query.select(feildsBy);
    } else {
      this.query = this.query.select("-__v");
    }

    return this;
  }
}



const getTop = (req, res, next) => {
  req.query.limit = "5";
  req.query.sort = "-ratingsAverage,price";
  req.query.fields = "name,price,difficulty,ratingsAverage,summary";
  //console.log("Middle")
  next();
};

const getAllData =CatchAsync( async (req, res,next) => {

  const filters = new APIFeatures(ToursDB.find(), req.query)
    .filter()
    .sort()
    .limitedFeilds()
    .pagination();
  const Toursall = await filters.query//.explain();
  // console.log(Toursall)

  res.status(200).json({
    message: "Get the data",
    result: Toursall.length,
    data: {
      Toursall,
    },
  });
});



const createTour =CatchAsync  (async (req, res,next) => {
  const newTours = await ToursDB.create(req.body)
    res.status(201).json({
      status: "success",
      data: {
        newTours,
      },
    });
 
});

const singleData =CatchAsync (  async (req, res,next) => {
  const { id } = req.params;
  
    const tourID = await ToursDB.findById(id).populate({path: "review", 
    })
  
    if( !tourID ){
      return  next(new AppError(`No found the ID`,404))
    }
    res.status(201).json({
       status:"Sucessfully",
      data: {
        tourID,
      },
    });

});

const updateTours = CatchAsync ( async (req, res,next) => {
  const { id } = req.params;
 
 
  const uptourID = await ToursDB.findByIdAndUpdate(id, req.body, {
    new: true,
    runValidators: true,
  });
  if(!uptourID){
    return  next(new AppError(`No found the ID`,404))
  }

  res.status(200).json({
    message: "Done Update",
    data: {
      uptourID,
    },
  });
});

const deleteTour =  MainFunction.deleteOne(ToursDB)



/*
const getTourStats = async (req, res) => {
  try {
    const stats = await ToursDB.aggregate([
      {
        $match: { ratingsAverage: { $gte: 4.5 } },
      },
      {
        $group: {
          _id: null,
          avgRating: { $avg: "$ratingsAverage" },
          avgPrice: { $avg: "$price" },
          minPrice: { $min: "$price" },
          maxPrice: { $max: "$price" },
        },
      },
    ]);
    res.status(200).json({
      status: "success",
      data: {
        stats,
      },
    });
  } catch (error) {
    res.status(404).json({
      status: "failed",
      message: error,
    });
  }
};
*/

/*
const getTourStats = async (req, res) => {
  try {
    const stats = await ToursDB.aggregate([
      {
        $match: { ratingsAverage: { $gte: 4.5 } },
      },
      
      {
        $group: {
          _id: { $toUpper: "$difficulty" },
          numTours: { $sum: 1 },
          numRatings: { $sum: "$ratingsQuantity" },
          avgRating: { $avg: "$ratingsAverage" },
          avgPrice: { $avg: "$price" },
          minPrice: { $min: "$price" },
          maxPrice: { $max: "$price" },
        },
      },
      {
        $sort: { avgPrice: 1 },
      },
    ]);
  console.log("---------",stats)
    res.status(200).json({
      status: "success",
      data: {
        stats,
      },
    });
  } catch (err) {
    res.status(404).json({
      status: "fail",
      message: err,
    });
  }
};
*/

const getMontlyTour=CatchAsync(async(req,res,next)=>{
   
  const year=req.params.year *1;
 

  const planMontly=await ToursDB.aggregate([
    {
      $unwind:'$startDates'
    },{
      $match:{
        startDates:{ 
          $gte:new Date(`${year}-01-01`),
          $lte:new Date(`${year}-12-31`)
        }
      }
    },{
      $group:{
        _id:{$month :'$startDates'},
        noofTours:{$sum:1},
        tours:{
          $push:'$name'
        }
      }
    },
    {
      $addFields:{month:'$_id'}
    },{
      $project:{
        _id:0
      }
    },{
      $sort:{
        noofTours:-1
      }
    },{
      $limit:12
    }
  ])

  res.status(200).json({
    message:"Year Plan",
    data:planMontly
  })

})

const nearMe=catchAsync(async(req,res,next)=>{
//  /tourNear/:distance/place/:latlon(lat,lon)
const {distance,latlon}=req.params
const [lan,lat]=latlon.split(",")

if(!lan || !lat){
 next(new AppError("Please provide longitude and latitude"))
}
  const radius=distance/6378.1
const data=await ToursDB.find({startLocation:{$geoWithin :{$centerSphere:[[lat,lan],radius]}}})

res.status(201).json({ 
  msg:"Succes in  km",
  length:data.length,
  data:data
})


})

module.exports = {
  getAllData,
  createTour,
  singleData,
  updateTours,
  deleteTour,
  getTop,
  getMontlyTour,
  nearMe
};
