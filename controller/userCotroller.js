const User = require("../model/userModel.js");
const CatchAsync = require("../utils/catchAsync.js");
const AppError = require("../utils/appError.js")
const MainFunction=require("../utils/handlerController.js");
const catchAsync = require("../utils/catchAsync.js");

let filterObj=(obj , ...allowFields)=>{
  const newObj={}
  Object.keys(obj).forEach(ele =>{
      if(allowFields.includes(ele)) newObj[ele]=obj[ele]
  })
 // console.log(newObj)
  return newObj
}

const getMe=(req,res,next)=>{
  req.params.id=req.user.id
  console.log("user",req.user.id)
  console.log("param",req.params.id)
  next()
}

const  updateMyData=CatchAsync(async (req,res,next)=>{
   // create error if the user post password
if(req.body.Password || req.body.comfirmPassword){
  return next(new AppError("This Routes is not for Upadte the  password",404))
}
if(req.body.role){
  return next(new AppError("You cannot update the role!!!!!",404))
}
   // update the user
   const filterField = filterObj(req.body,"name","email")
   const updateUser= await User.findByIdAndUpdate(req.user.id,filterField,{
    new:true,
    runValidators:true,
   })
   

   res.status(200).json({
    statua:"Pass",
    user:updateUser
   })
})

const getAllUser =CatchAsync( async (req, res) => {
 
    const allUser = await User.find();
    res.status(200).json({
      status: "Passed",
      msg: "fulfilled",
      Length:allUser.length,
      data: {
        allUser,
      },
    });
  
});


const deleteMany=CatchAsync(async (req,res,next)=>{
   await User.deleteMany()

   res.json({
    msg:"Deleted"
   })
}
)

const deleteMe= MainFunction.deleteOne(User)

const getUser=catchAsync(async (req,res,next)=>{
  const {id}=req.params
  const user=await User.findById(id)
  if(!user){
    throw new Error("user not found")
  }
  res.status(200).json({
    status:"User",
    data:{
      user
    }
  })
})

const deleteUser=MainFunction.deleteOne(User)


module.exports = { getUser, getAllUser,deleteMany,updateMyData ,deleteMe,getMe,deleteUser};
