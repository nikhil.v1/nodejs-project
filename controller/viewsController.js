const catchAsync=require("../utils/catchAsync")
const TourDB=require("../model/toursModel")

exports.getOverView=catchAsync(async(req,res,next)=>{
 const tours=await TourDB.find()

 res.status(200).render('overview',{
	title:"All Tours",
	tours
 })

})

exports.getTour=catchAsync(async(req,res,next)=>{
	const tour=await TourDB.findOne({slug:req.params.slug}).populate({
		path:"reviews",
		select:"review rating user"
	})
	res.status(200).render("tour",{
		title:`${tour.name} Tour`,
		tour
	})
})

exports.getLoginFrom=(req,res)=>{
  res.status(200).render("login",{
		title:"Login Into Account"
	})
}