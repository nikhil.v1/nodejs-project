const monogoose =require("mongoose")
const fs=require("fs")
const dotnet = require("dotenv")
const ToursDB=require("./model/toursModel.js")
const userDB=require("./model/userModel.js")
const reviewDB=require("./model/reviewModel.js")
dotnet.config({ path :"./.env"})

const connectDB = async () => {
    try {
        await monogoose.connect(process.env.DATABASE);
        console.log("Successfully connected to the database");
    } catch (error) {
        console.error("Error connecting to the database: ", error);
        process.exit(1);
    }
};

connectDB();

//reading the file 
const tours  = JSON.parse(fs.readFileSync("./data/tours.json",'utf8'))
const review=JSON.parse(fs.readFileSync("./data/reviews.json",'utf8'))
const user=JSON.parse(fs.readFileSync("./data/users.json",'utf8'))



//import 
const importData = async (req,res)=>{
  try {
    await ToursDB.create(tours)
    // await reviewDB.create(review)
    // await userDB.create(user,{validateBeforeSave:false})
    console.log("Data is Import Successfully")
  } catch (error) {
    console.log(`Error from import Data: ${error}`)
  }
  process.exit()
}


const deleteData= async (req,res)=>{
    try {
      await ToursDB.deleteMany() 
      // await reviewDB.deleteMany()
      // await userDB.deleteMany()
      console.log("Delete the data successfully") 
      
    } catch (error) {
        console.log(`Error from delete Data: ${error}`)
    }
    process.exit()
}


if(process.argv[2] === "--import"){
    importData()
   
}else if(process.argv[2] === "--delete"){
    deleteData()
}
console.log(process.argv);
