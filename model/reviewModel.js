const mongoose = require("mongoose");
const ToursDB = require("./toursModel");

const reviewSchema = new mongoose.Schema(
  {
    review: {
      type: String,
      required: [true, "Review cannot be empty"],
    },
    rating: {
      type: Number,
      min: 1,
      max: 5,
    },
    createdAt: {
      type: Date,
      default: Date.now,
    },
    tour: {
      type: mongoose.Schema.ObjectId,
      ref: "ToursDB",
      required: [true, "Review must belong to a tour"],
    },
    user: {
      type: mongoose.Schema.ObjectId,
      ref: "User",
      required: [true, "Review must belong to a user"],
    },
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

reviewSchema.pre(/^find/, function (next) {
  this.populate({
    path: "user",
    select: "name email photo",
  });
  next();
});

reviewSchema.statics.calculateRating = async function (tourID) {
  const stat = await this.aggregate([
    {
      $match: {
        tour: tourID,
      },
    },
    {
      $group: {
        _id: "$tour",
        noofRating: { $sum: 1 },
        avgRating: { $avg: "$rating" },
      },
    },
  ]);

  if (stat.length > 0) {
    await ToursDB.findByIdAndUpdate(tourID, {
      ratingsAverage: stat[0].avgRating,
      ratingsQuantity: stat[0].noofRating,
    });
  } else {
    await ToursDB.findByIdAndUpdate(tourID, {
      ratingsAverage: 0,
      ratingsQuantity: 0,
    });
  }
};
reviewSchema.index({tour:1,user:1},{unique:true})
reviewSchema.post("save", function () {
  console.log(this.constructor)
  this.constructor.calculateRating(this.tour);
});

//for update and delete the review the avg should also change
//start
reviewSchema.pre(/^findOneAnd/, async function (next) {
  this.reviewCurr = await this.model.findOne(this.getQuery());
  next();
});

reviewSchema.post(/^findOneAnd/, async function () {
  await this.reviewCurr.constructor.calculateRating(this.reviewCurr.tour);
});
// end
const review = mongoose.model("review", reviewSchema);

module.exports = review;
