const mongoose = require("mongoose");
var slugify = require("slugify");
//const User=require("./userModel")

const toursSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      // unique: true,
      required: [true, "Enter the Name of the tours"],
    },
    slug: String,
    duration: {
      type: String,
      required: [true, "Enter the duration of the tours"],
    },
    maxGroupSize: {
      type: Number,
      required: [true, "Enter the group size"],
    },
    difficulty: {
      type: String,
      required: [true, "Enter the difficult level"],
    },
    ratingsAverage: {
      type: Number,
      default: 0,
      min:[0,"Rating should be equal or greater then 0" ],
      max:[5,"Rating should be equal or less then 5" ],
      set:val=> Math.round(val *10)/10,
    },
    ratingsQuantity: {
      type: Number,
      default: 0,
    },
    price: {
      type: Number,
      required: [true, "Enter the prices"],
    },
    priceDiscount: Number,
    summary: {
      type: String,
      trim: true,
      required: [true, "Enter the proper description"],
    },
    description: {
      type: String,
      trim: true,
    },
    imageCover: {
      type: String,
      required: [true, "Enter the cover image"],
    },
    images: [String],
    createdAt: {
      type: Date,
      default: Date.now(),
    },
    startDates: [Date],
    secretTour: {
      type: Boolean,
      default: false,
    },
    startLocation: {
      type: {
        type: String,
        default: "Point",
        enum: ["Point"],
      },
      coordinates: [Number],
      address: String,
      description: String,
    },
    locations: [
      {
        type: {
          type: String,
          default: "Point",
          enum: ["Point"],
        },
        coordinates: [Number],
        description: String,
        day: Number,
      },
    ],
    guides: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
      },
    ],
    startLocation:{
      //GeoJson
      type:{
        type:String,
        default:'Point',
        enum:['Point']
      },
      coordinates:[Number],
      addrress:String,
      description:String
    },
    locations:[{
      //GeoJson
      type:{
        type:String,
        default:'Point',
        enum:['Point']
      },
      coordinates:[Number],
      addrress:String,
      description:String,
      day:Number
    }]
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

toursSchema.virtual("durationWeek").get(function () {
  return this.duration / 7;
});

toursSchema.virtual("reviews",{
  ref:"review",
  foreignField:"tour",
  localField:"_id"
})

// middleware
toursSchema.pre("save", function (next) {
  this.slug = slugify(this.name, { lower: true }); // lowercaes
  next();
});

// toursSchema.pre("save", async function (next) {
//   const guidesPromise = this.guides.map(async (id) => await User.findById(id));
//   this.guides = await Promise.all(guidesPromise);
//   next();
// });

//query middleware
toursSchema.pre(/^find/, function (next) {
  this.find({ secretTour: { $ne: true } });
  this.start=Date.now()
  next();
});

toursSchema.pre(/^find/,function(next){
  this.populate({
    path:'guides',
    select:'-__v -passwordChangeAT'
  })
  next()
})

toursSchema.post(/^find/, function(docs, next) {
  console.log(`Query took ${Date.now() - this.start} milliseconds!`);
  next();
});

toursSchema.index({price:1,ratingsAverage:1})
toursSchema.index({slug:1})

//  /^find/ will apply  all the query which container find words

const ToursDB = mongoose.model("ToursDB", toursSchema);

module.exports = ToursDB;
