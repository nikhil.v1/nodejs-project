const mongoose = require("mongoose");
const validator = require("validator");
const bcrypt = require("bcrypt");
const crypto = require("crypto");

//name,email,photo,pwd,pwdCom

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Please enter the Name"],
  },
  passwordChangeAT: Date,
  email: {
    type: String,
    required: [true, "Please enter the email"],
    unique: true,
    lowercase: true,
    validator: [validator.isEmail, "Please enter the vaild email "],
  },
  photo: String,
  role: {
    type: String,
    enum: {
      values: ["user", "admin", "team-lead", "super-admin"],
      message: "Enter the correct role",
    },
    default: "user",
  },
  Password: {
    type: String,
    required: [true, "Please enter the password"],
    minLength: 8,
    select: false,
  },
  comfirmPassword: {
    type: String,
    required: [true, "Plese re enter the password"],
    validate: {
      validator: function (pwd) {
        return pwd === this.Password;
      },
      message: "Please Re-enter the  password",
    },
  },
  passwordRestToken: String,
  passwordRestExpried: Date,
  active: {
    type: Boolean,
    default: true,
    select: false,
  },
});

//cryppt
userSchema.pre("save", async function (next) {
  if (!this.isModified("Password")) return next();

  this.Password = await bcrypt.hash(this.Password, 12);
  this.comfirmPassword = undefined;
  next();
});

userSchema.pre("save", function (next) {
  if (!this.isModified("Password") || this.isNew) return next();

  this.passwordChangeAT = Date.now() - 1000;
  next();
});

userSchema.pre(/^find/, function (next) {
  this.find({ active: { $ne: false } });
  next();
});

userSchema.methods.correctPassword = async function (candinatePwd, UserPwd) {
  return await bcrypt.compare(candinatePwd, UserPwd);
};

userSchema.methods.passwordChanged = function (TIMESTAMP) {
  if (this.passwordChangeAT) {
    const changeTimeStamp = parseInt(
      this.passwordChangeAT.getTime() / 1000,
      10
    );
    // console.log(changeTimeStamp,TIMESTAMP)
    return TIMESTAMP < changeTimeStamp;
  }

  return false;
};

userSchema.methods.restpwdToken = function () {
  const restToken = crypto.randomBytes(32).toString("hex");

  this.passwordRestToken = crypto
    .createHash("sha256")
    .update(restToken)
    .digest("hex");
  this.passwordRestExpried = Date.now() + 10 * 60 * 1000;
  // console.log({ restToken }, this.passwordRestToken)
  return restToken;
};

const User = mongoose.model("User", userSchema);

module.exports = User;
