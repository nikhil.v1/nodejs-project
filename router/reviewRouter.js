const express = require("express");
const router = express.Router({ mergeParams: true }); ///Get /tour/:id/review this mergeparam:true is get the :id
const reviewController = require("../controller/reviewController.js");
const { protectAll, restrictTo } = require("../controller/authController.js");

router.use(protectAll)

router
  .route("/")
  .get(reviewController.getAllReview)
  .post( restrictTo("user", "admin"), reviewController.setTourUserIds,reviewController.addReview);

router
  .route("/:id")
  .get(reviewController.getSingle)
  .delete(
    restrictTo("user", "admin"),
    reviewController.removeReview
  )
  .put( restrictTo("user", "admin","super-admin"),reviewController.updateReview);

module.exports = router;
