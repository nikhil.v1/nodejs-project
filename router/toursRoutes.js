const express = require("express");
const router = express.Router();
const {
  getAllData,
  createTour,
  singleData,
  updateTours,
  deleteTour,
  getTop,
  getMontlyTour,
  nearMe,
  // getTourStats,
} = require("../controller/tourController.js");
//const reviewController=require("../controller/reviewController.js")
const { protectAll, restrictTo } = require("../controller/authController.js");
const reviewRouter = require("../router/reviewRouter.js");

//Post /tour/:id/review
//Get /tour/:id/review
//Get /tour/:id/review/:id

//router.route("/:tourId/reviews").post(protectAll,restrictTo("user"),reviewController.addReview)

router.use("/:tourId/reviews", reviewRouter);

router.route("/top-5").get(getTop, getAllData);
router.route("/").get(getAllData);
router.route("/getMonthly/:year").get(getMontlyTour);
router.route("/tourNear/:distance/place/:latlon").get(nearMe)
router
  .route("/")
  .post(protectAll, restrictTo("admin", "team-lead","super-admin"), createTour);
router.route("/:id").get(singleData);
router
  .route("/:id")
  .put(protectAll, restrictTo("admin", "team-lead","super-admin"), updateTours);
router
  .route("/:id")
  .delete(protectAll, restrictTo("admin", "team-lead","super-admin"), deleteTour);

//router.route('/top').get(getTourStats);

module.exports = router;
