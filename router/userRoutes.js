const express = require("express");
const router = express.Router();
const {
  getAllUser,
  deleteMany,
  updateMyData,
  deleteMe,
  getUser,
  getMe,
  deleteUser,
} = require("../controller/userCotroller.js");
const {
  signup,
  login,
  forgotPwd,
  restPwd,
  protectAll,
  updatePasword,
  restrictTo,
} = require("../controller/authController.js");

router.route("/signup").post(signup);
router.route("/login").post(login);
router.route("/forgot").post(forgotPwd);
router.route("/rest/:token").patch(restPwd);

//by using here "protectAll" below all the routes the "protectAll" is applyied
router.use(protectAll);

router.route("/updateData").patch(updateMyData);
router.route("/deleteMe").delete(deleteMe);
router.route("/me").get(getMe, getUser);
router.route("/updatepassword").patch(updatePasword);

router.use(restrictTo("admin","super-admin"));

router.route("/all").get(getAllUser);
router.route("/:id").get(getUser).delete(deleteUser);

router.route("/deleteAll").delete(restrictTo("super-admin"), deleteMany);

module.exports = router;
