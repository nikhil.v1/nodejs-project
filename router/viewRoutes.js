const express = require('express');
const viewsController = require('../controller/viewsController');
const router = express.Router();
const {protectAll}=require("../controller/authController")

router.get("/",viewsController.getOverView)

router.get("/tour/:slug",protectAll,viewsController.getTour)

//  /login 
router.get("/login",viewsController.getLoginFrom)


module.exports = router;
