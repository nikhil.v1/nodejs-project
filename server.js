const express = require("express");
const path=require("path")
const morgan = require("morgan");
const cookieParser=require("cookie-parser")
const mongoSanitize=require("express-mongo-sanitize")
const tourRoutes = require("./router/toursRoutes.js");
const AppEror = require("./utils/appError.js");
const globalError = require("./controller/errorControler.js");
const userRoutes = require("./router/userRoutes.js");
const rateLimit = require("express-rate-limit");
const helmet = require("helmet");
const reviewRoutes=require("./router/reviewRouter.js")
const viewRoutes=require("./router/viewRoutes.js")
const app = express();

app.use(express.json());
app.set("view engine","pug")
app.set("views",path.join(__dirname,"views"))
app.use(express.static(path.join((__dirname,`public`))));
app.use(express.urlencoded({ extended: false }));
app.use(morgan("dev"));
//set security http header
app.use(helmet());


const limiter = rateLimit({
  max: 100,
  windowMs: 60 * 60 * 1000,
  messge: "To many request, please try after some hours",
});

app.use("/api", limiter);
app.use(mongoSanitize())
app.use(cookieParser())

app.use((req, res, next) => {
  req.requestTime = new Date().toISOString();
  
  next();
});


 app.use("/",viewRoutes)
app.use("/api/v1/tours", tourRoutes); // starting point tours
app.use("/api/v1/users", userRoutes);
app.use("/api/v1/reviews", reviewRoutes);

app.all("*", (req, res, next) => {
  next(new AppEror(`Enter the correct URL:${req.originalUrl} `, 404));
});

app.use(globalError);

module.exports = app;
