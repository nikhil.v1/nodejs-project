const monogoose =require("mongoose")
const app = require("./server.js")
const dotnet = require("dotenv")

dotnet.config({ path :"./.env"})

monogoose.connect(process.env.DATABASE)
.then(()=>{
    console.log("Sucessfully Connect Databases")
})
.catch((e)=>{
    console.log("Error to databases" +e)
})

//start  file

const port =3000


app.listen(port, () => console.log(`Example app listening on port ${port}!`));
