const nodemailer = require("nodemailer");
const sendEmail = async (options) => {
  //create a transporter
  const transporter = nodemailer.createTransport({
    host: process.env.EMAIL_HOST,
    port: process.env.EMAIL_PORT,
    auth: {
      user: process.env.EMAIL_USERNAME,
      pass: process.env.EMAIL_PWD
    },
  });
  //define a mail options
  const mailOptions = {
    from: " Nikhil Varma  nikhilvarma@gmail.com",
    to: options.email,
    subject: options.subject,
    text: options.message,
  };
  //actualliu send the mail
  
 await transporter.sendMail(mailOptions);
 
};


module.exports=sendEmail

