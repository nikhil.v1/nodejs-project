const catchAsync = require("./catchAsync");
const AppError=require("./appError")

exports.deleteOne = (Model) =>
  catchAsync(async (req, res, next) => {
    const { id } = req.params;
    const deletData = await Model.findByIdAndDelete(id);
    if (!deletData) {
      return next(new AppError(`No found the ID`, 404));
    }
    res.status(202).json({
      message: "Delete done",
      result:null
    });
}); 
